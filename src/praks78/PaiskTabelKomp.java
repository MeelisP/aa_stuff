package praks78;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Meelis on 16/10/2017.
 */
public class PaiskTabelKomp {
    List<Integer> räsi = new ArrayList<>();
    int elementideArv = 0;

    public PaiskTabelKomp(int n) {
        for (int i = 0; i < n; i++) {
            räsi.add(0);
        }
    }

    public void lisaTootaja(int ID){
        int kompeSamm = 1;
        int räsiSuurus = räsi.size();
        int uueIndeks = ID % räsiSuurus;
        while (true) {
            if (räsi.get(uueIndeks % räsiSuurus) == 0) {
                räsi.set(uueIndeks, ID);
                elementideArv++;
                break;
            } else
                uueIndeks += kompeSamm;

            if (elementideArv == räsiSuurus)
                throw new RuntimeException("Paisktabel sai täis");
        }
    }

    @Override
    public String toString() {
        return "PaiskTabelKomp{" +
                "räsi=" + räsi +
                '}';
    }
}
