import java.util.ArrayList;
import java.util.List;

/**
 * Created by Meelis on 04/09/2017.
 */
public class main {

    public static List<Long> fibonacciList = new ArrayList<>();

    public static void main(String[] args) {
        fibonacciList.add((long) 1);
        fibonacciList.add((long) 1);
        long start = System.currentTimeMillis();
        System.out.println(fibo2(100));
        long end = System.currentTimeMillis();
        System.out.println("time " + (end - start) + "ms");
    }

    public static int fibo(int n) {
        if (n == 0) return 0;
        if (n == 1) return 1;
        return fibo(n - 1) + fibo(n - 2);
    }

    public static Long fibo2(int n) {
        while (fibonacciList.size() <= n)
            updateList();
        return fibonacciList.get(n);
    }

    public static void updateList() {
        int size = fibonacciList.size();
        System.out.println(fibonacciList.get(size - 1));
        fibonacciList.add(fibonacciList.get(size - 1) + fibonacciList.get(size - 2));
    }


}
