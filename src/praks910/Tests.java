package praks910;

import org.junit.Assert;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class Tests {

    @Test
    public void kopLisamine() {

        KahendOtsimispuu<Integer> tulemus = leht(9).lisaKirje(5).lisaKirje(7).lisaKirje(8).lisaKirje(1);

        KahendOtsimispuu<Integer> eeldus = haru(9,
                haru(5, leht(1), haru(7, null, leht(8))),
                null);

        assertEquals(tulemus.sulgEsitus(), eeldus.sulgEsitus());
    }

    @Test
    public void kopKustutamine() {

        KahendOtsimispuu<Integer> tulemus = leht(9).lisaKirje(5).lisaKirje(7).lisaKirje(8).lisaKirje(1);
        tulemus.eemaldaKirje(1);
        KahendOtsimispuu<Integer> eeldatav = haru(9,
                haru(5, null, haru(7, null, leht(8))),
                null
        );

        assertEquals(eeldatav.sulgEsitus(), tulemus.sulgEsitus());

        tulemus.eemaldaKirje(5);
        eeldatav = haru(9,
                haru(7, null, leht(8)),
                null
        );

        assertEquals(eeldatav.sulgEsitus(), tulemus.sulgEsitus());

        tulemus.eemaldaKirje(9);
        eeldatav = haru(8,
                haru(7, null, null),
                null
        );

        assertEquals(eeldatav.sulgEsitus(), tulemus.sulgEsitus());
    }

    @Test
    public void testParemPööre() {
        Tipp<Integer> alguses = haru(6, haru(2, leht(1), haru(4, leht(3), leht(5))), haru(8, leht(7), leht(9)));

        Tipp<Integer> eeldatav = haru(2, leht(1), haru(6, haru(4, leht(3), leht(5)), haru(8, leht(7), leht(9))));

        assertEquals(eeldatav.sulgEsitus(), alguses.paremPööre().sulgEsitus());
    }

    @Test
    public void testVasakPööre() {
        Tipp<Integer> alguses = haru(6, haru(2, leht(1), haru(4, leht(3), leht(5))), haru(8, leht(7), leht(9)));

        Tipp<Integer> eeldatav = haru(8, haru(6, haru(2, leht(1), haru(4, leht(3), leht(5))), leht(7)), leht(9));

        assertEquals(eeldatav.sulgEsitus(), alguses.vasakPööre().sulgEsitus());
    }

    @Test
    public void testParemVasakPööre() {
        Tipp<Integer> alguses = haru(6, haru(2, leht(1), haru(4, leht(3), leht(5))), haru(8, leht(7), leht(9)));

        Tipp<Integer> eeldatav = haru(7, haru(6, haru(2, leht(1), haru(4, leht(3), leht(5))), null), haru(8, null, leht(9)));

        assertEquals(eeldatav.sulgEsitus(), alguses.paremVasakPööre().sulgEsitus());
    }

    @Test
    public void testVasakParemPööre() {
        Tipp<Integer> alguses = haru(6, haru(2, leht(1), haru(4, leht(3), leht(5))), haru(8, leht(7), leht(9)));

        Tipp<Integer> eeldatav = haru(4, haru(2, leht(1), leht(3)), haru(6, leht(5), haru(8, leht(7), leht(9))));

        assertEquals(eeldatav.sulgEsitus(), alguses.vasakParemPööre().sulgEsitus());
    }

    @Test
    public void avlLisamine() {

        // LR CASE
        AVLKahendOtsimispuu<Integer> tulemus = leht(10).lisaKirjeAVL(5).lisaKirjeAVL(7).lisaKirjeAVL(8).lisaKirjeAVL(1);

        AVLKahendOtsimispuu<Integer> eeldatav = haru(7,
                haru(5, leht(1), null),
                haru(10, leht(8), null)
        );

        assertEquals(eeldatav.sulgEsitus(), tulemus.sulgEsitus());

        // Slaidide järgi tehtud test

        tulemus = leht(40).lisaKirjeAVL(35).lisaKirjeAVL(36).lisaKirjeAVL(60).lisaKirjeAVL(44).lisaKirjeAVL(41).lisaKirjeAVL(55).lisaKirjeAVL(44).lisaKirjeAVL(43).lisaKirjeAVL(45);

        eeldatav = haru(44, haru(40, haru(36, leht(35), null), haru(41, null, leht(43))), haru(55, haru(44, null, leht(45)), leht(60)));

        assertEquals(eeldatav.sulgEsitus(), tulemus.sulgEsitus());

    }

    @Test
    public void testAvlLisamineEemaldamine() {

        //slaidide põhjal tehtud
        AVLKahendOtsimispuu<Integer> tulemus = haru(30, haru(15, leht(10), haru(20, null, leht(25))),
                haru(55, haru(40, leht(35), haru(50, leht(45), null)), haru(70, haru(60, null, leht(65)), leht(75))));

        AVLKahendOtsimispuu<Integer> etapp = haru(30, haru(15, leht(10), haru(20, null, leht(25))),
                haru(55, haru(40, leht(35), haru(50, leht(45), null)), haru(65, leht(60), leht(70))));
        tulemus.eemaldaKirjeAVL(75);
        assertEquals(etapp.sulgEsitus(), tulemus.sulgEsitus());

        etapp = haru(40, haru(30, haru(20, leht(15), leht(25)), leht(35)), haru(55, haru(50, leht(45), null), haru(65, leht(60), leht(70))));
        tulemus.eemaldaKirjeAVL(10);
        assertEquals(etapp.sulgEsitus(), tulemus.sulgEsitus());

        etapp = haru(40,
                haru(20, leht(15), haru(35, leht(25), null)),
                haru(55, haru(50, leht(45), null), haru(65, leht(60), leht(70))));
        tulemus.eemaldaKirjeAVL(30);
        assertEquals(etapp.sulgEsitus(), tulemus.sulgEsitus());

        etapp = haru(40,
                haru(20, leht(15), leht(25)),
                haru(55, haru(50, leht(45), null), haru(65, leht(60), leht(70))));
        tulemus.eemaldaKirjeAVL(35);
        assertEquals(etapp.sulgEsitus(), tulemus.sulgEsitus());

        etapp = haru(40,
                haru(25, leht(15), null),
                haru(55, haru(50, leht(45), null), haru(65, leht(60), leht(70))));
        tulemus.eemaldaKirjeAVL(20);
        assertEquals(etapp.sulgEsitus(), tulemus.sulgEsitus());

        etapp = haru(40,
                haru(25, leht(15), null),
                haru(60, haru(50, leht(45), null), haru(65, null, leht(70))));
        tulemus.eemaldaKirjeAVL(55);
        assertEquals(etapp.sulgEsitus(), tulemus.sulgEsitus());

        etapp = haru(60,
                haru(40, leht(15), haru(50,leht(45),null)),
                haru(65, null, leht(70)));
        tulemus.eemaldaKirjeAVL(25);
        assertEquals(etapp.sulgEsitus(), tulemus.sulgEsitus());

    }


    private Tipp<Integer> haru(Integer e, Tipp<Integer> v, Tipp<Integer> p) {
        return new Tipp<Integer>(e, v, p);
    }

    private Tipp<Integer> leht(Integer e) {
        return new Tipp<Integer>(e);
    }


}
