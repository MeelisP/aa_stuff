package praks910;

public class Main {

    public static void main(String[] args) {
        Tipp<Integer> puu = Tipp.juhuslikPuu(3);
        System.out.println(puu.eesjärjestus());
        System.out.println(puu.sulgEsitus());
        System.out.println(puu.tippudeArv());
        System.out.println(puu.kõrgus());
        puu = Tipp.täidaArvudega(puu);
        System.out.println(puu.sulgEsitus());
        System.out.println(puu.toString());
    }
}
