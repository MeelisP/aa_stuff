package praks34;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Meelis on 19/09/2017.
 */
public class ül316 {

    public static void main(String[] args) {
        System.out.println(lahutused(3, "", new ArrayList<String>()));
    }

    public static List<String> lahutused(int n, String p, List<String> l) {
        if (n == 0){
            l.add(p);
            return l;
        }
        if (n > 0) {
            lahutused(n - 1, p + " 1 ", l);
        }if (n > 1) {
            lahutused(n - 2, p + " 2 ", l);
        }
        return l;
    }
}
