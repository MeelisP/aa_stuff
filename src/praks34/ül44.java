package praks34;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Meelis on 19/09/2017.
 */
public class ül44 {
    public static void main(String[] args) {
        List<Integer> myndid = new ArrayList<>();
        myndid.add(1);
        myndid.add(2);
        myndid.add(3);
        myndid.add(4);
        System.out.println(raha(myndid, 0, 5, 0));

    }

    public static boolean raha(List<Integer> myndid, int i, int s, int currentS) {
        if (currentS == s) {
            return true;
        }

        if (currentS > s)
            return false;
        int mynt = 0;
        try {
            mynt = myndid.get(i);
        } catch (RuntimeException e) {
            return false;
        }
        i++;

        return raha(myndid, i, s, currentS + 2 * mynt) || raha(myndid, i, s, currentS + mynt) || raha(myndid, i, s, currentS);
    }
}
