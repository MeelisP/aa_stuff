package praks34;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Meelis on 19/09/2017.
 */
public class bitivektorid2 {

    public static void main(String[] args) {
        System.out.println(genereeriBitiVektoreid("", 10, 2, new ArrayList<>()));
    }

    public static List<String> genereeriBitiVektoreid(String prefix, int n, int k, List<String> list) {
        if (n == 0) {
            list.add(prefix);
            return list;
        }
        if (k < n)
            genereeriBitiVektoreid(prefix + "0", n - 1, k, list);
        genereeriBitiVektoreid(prefix + "1", n - 1, k - 1, list);
        return list;
    }
}
