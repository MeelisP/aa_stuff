package praks34;

public class tkHarjutus1 {

    public static void main(String[] args) {
        bitiVektorid(3, "");
    }

    public static void bitiVektorid(int n, String eesliide) {

        if (n == 0) {
            System.out.println(eesliide);
            return;
        }

        bitiVektorid(n - 1, eesliide + "1");
        bitiVektorid(n - 1, eesliide + "0");
    }
}
