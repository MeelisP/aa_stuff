package praks34;

import java.util.ArrayList;
import java.util.List;
import java.util.Queue;
import java.util.Stack;

/**
 * Created by Meelis on 25/09/2017.
 */
public class ül511 {
    public static void main(String[] args) {
        System.out.println(saaTehted("(((a+b) ∗ (c−d ∗ k) +2.5)/(pi ∗ (1−e)))", 1, new ArrayList<>()));
    }

    public static List<String> saaTehted(String tehe, int tehteIndeks, List<String> tehted) {
        Stack<Integer> esimesedSulud = new Stack<>();
        tehe = tehe.replace(" ", "");

        //sulud
        for (int i = 0; i < tehe.length(); i++) {
            if (tehe.substring(i, i + 1).equals("(")) {
                esimesedSulud.add(i + 1);
            }

            else if (tehe.substring(i, i + 1).equals(")")) {
                int suluindex = esimesedSulud.pop();
                String uustehe = tehe.substring(0, suluindex - 1) + "S" + tehteIndeks + tehe.substring(i + 1);
                tehted.add("S" + tehteIndeks + " := " + tehe.substring(suluindex, i));
                tehteIndeks++;
                System.out.println(uustehe);
                saaTehted(uustehe, tehteIndeks, tehted);
                break;
            }
        }
        return tehted;
    }
}
