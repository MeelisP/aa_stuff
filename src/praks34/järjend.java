package praks34;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Meelis on 25/09/2017.
 */
public class järjend {
    public static void main(String[] args) {
        List<String> järjend = new ArrayList<>();
        järjend.add("a");
        järjend.add("b");
        järjend.add("c");
        kombinatsioonid(2, järjend, "");
    }

    public static void kombinatsioonid(int k, List<String> järjend, String sõne) {
        if (k == 0) {
            System.out.println(sõne);
            return;
        }
        List<String> uusjärjend = new ArrayList<>();
        uusjärjend.addAll(järjend);
        for (String s : järjend) {
            uusjärjend.remove(s);
            kombinatsioonid(k - 1, uusjärjend, sõne + s);
            uusjärjend.add(s);
        }
        return;
    }
}
