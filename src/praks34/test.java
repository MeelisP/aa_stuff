package praks34;

import java.util.ArrayList;
import java.util.List;

public class test {

    public static void main(String[] args) {
        List<Integer> raamatud = new ArrayList<>();
        raamatud.add(62);
        raamatud.add(10);
        raamatud.add(3);
        System.out.println(f(0, raamatud, 0));
    }

    public static int f(int h, List<Integer> r, int i) {
        int a = 0;

        if (h > 100 || r.size() == i)
            return 0;

        if (h + r.get(i) >= 50 && h + r.get(i) <= 100)
            a = 1;

        return a + f(h + r.get(i), r, i + 1) + f(h, r, i + 1);
    }
}
