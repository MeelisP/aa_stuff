package praks34;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Meelis on 19/09/2017.
 */
public class ül412 {
    public static void main(String[] args) {
        System.out.println(trepp(3, new ArrayList<>(), ""));
    }

    public static List<String> trepp(int n, List<String> tulemus, String rada) {
        if (n == 0) {
            tulemus.add(rada);
            return tulemus;
        }
        if (n > 0) {
            trepp(n - 1, tulemus, rada + "1");
        } if (n > 1) {
            trepp(n - 2, tulemus, rada + "2");
        } if (n > 2) {
            trepp(n - 3, tulemus, rada + "3");
        }
        return tulemus;
    }

}
