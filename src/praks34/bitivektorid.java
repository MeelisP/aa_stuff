package praks34;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Created by Meelis on 18/09/2017.
 */
public class bitivektorid {

    public static void main(String[] args) {
        System.out.println(genereeriBitiVektoreid("", 3, new ArrayList<>()));
    }

    public static List<String> genereeriBitiVektoreid(String prefix, int n, List<String> list) {
        if (n == 0) {
            list.add(prefix);
            return list;
        }
        genereeriBitiVektoreid(prefix + "0", n - 1, list);
        genereeriBitiVektoreid(prefix + "1", n - 1, list);
        return list;
    }
}
