package praks34;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Meelis on 18/09/2017.
 */
public class permutatsioonid {

    public static void main(String[] args) {

        long start = System.currentTimeMillis();
        for (String arg : s6nePerm("", "12345678", new ArrayList<>())) {
            System.out.println(arg);
        }
        long end = System.currentTimeMillis();
        System.out.println("time: " + (end - start) + "ms");

    }

    public static List<String> s6nePerm(String prefix, String s6ne, List<String> tulemus) {
        if (s6ne.length() == 0) {
            tulemus.add(prefix);
            return tulemus;
        }

        for (int i = 0; i < s6ne.length(); i++) {
            String s = s6ne.substring(i, i + 1);
            String nS6ne = s6ne.substring(0, i) + s6ne.substring(i + 1, s6ne.length());
            s6nePerm(prefix + s, nS6ne, tulemus);
        }
        return tulemus;
    }
}
