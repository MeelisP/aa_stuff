package praks56;

import java.util.*;

/**
 * Created by Meelis on 02/10/2017.
 */
public class ül3 {

    public static void main(String[] args) {
        System.out.println(PoolaKalk("2 3 * 4 + 5 /"));
    }

    public static int PoolaKalk(String tehe) {
        List<String> list = new ArrayList<String>(Arrays.asList(tehe.split(" ")));
        Stack<String> kuhi = new Stack<>();
        Collections.reverse(list);
        for (String s : list) {
            kuhi.push(s);
        }

        int esimeneArv = Integer.parseInt(kuhi.pop());
        while (kuhi.size() > 1) {
            int teineArv = Integer.parseInt(kuhi.pop());
            String tehteMärk = kuhi.pop();
            if (tehteMärk.equals("+")) {
                esimeneArv += teineArv;
            } else if (tehteMärk.equals("-")) {
                esimeneArv -= teineArv;
            } else if (tehteMärk.equals("*")) {
                esimeneArv *= teineArv;
            } else if (tehteMärk.equals("/")) {
                esimeneArv /= teineArv;
            }
        }
        return esimeneArv;
    }
}
