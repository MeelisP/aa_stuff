package praks56;

import java.awt.*;
import java.util.Stack;

public class male {

    public static void main(String[] args) {
        lipud(2);
    }

    public static int lipud(int lauaSuurus) {
        int voimalusi = 0;
        Stack<Point> asukohad = new Stack<>();
        boolean lopp = false;

        while (!lopp) {

            for (int x = asukohad.size(); x < lauaSuurus; x++) {
                for (int y = 0; y < lauaSuurus; y++) {
                    if (asukohad.empty()) {
                        asukohad.add(new Point(0, 0));
                        if (y == lauaSuurus - 1){
                            lopp = true;
                        }
                        break;
                    }
                    for (Point point : asukohad) {
                        if (!onLipuTuleAll(point, new Point(x, y), lauaSuurus)) {
                            asukohad.add(new Point(x, y));
                            x++;
                            y = 0;
                            break;
                        }
                    }

                    if (x == lauaSuurus - 1 && y == lauaSuurus - 1) {
                        Point viimane = asukohad.pop();
                        voimalusi++;
                        x = viimane.x;
                        y = viimane.y;
                        if (y == lauaSuurus - 1 && x == lauaSuurus - 1)
                            break;
                        else if (y == lauaSuurus - 1) {
                            x = viimane.x + 1;
                            y = 0;
                        }
                    }
                }
            }
            asukohad = new Stack<>();
            voimalusi++;
        }
        return voimalusi;
    }

    public static boolean onLipuTuleAll(Point lipuAsukoht, Point asukoht2, int lauaSuurus) {
        if (lipuAsukoht.x == asukoht2.x || lipuAsukoht.y == asukoht2.y)
            return true;
        //diagonaalide kontroll
        for (int i = 0; i < lauaSuurus; i++) {
            if     ((lipuAsukoht.x + i == asukoht2.x && lipuAsukoht.y + 1 == asukoht2.y) ||
                    (lipuAsukoht.x - 1 == asukoht2.x && lipuAsukoht.y - 1 == asukoht2.y) ||
                    (lipuAsukoht.x - 1 == asukoht2.x && lipuAsukoht.y + 1 == asukoht2.y) ||
                    (lipuAsukoht.x + 1 == asukoht2.x && lipuAsukoht.y - 1 == asukoht2.y))
                return true;
        }
        return false;
    }
}
