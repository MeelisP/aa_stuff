package homework.k7;

import java.io.*;
import java.util.ArrayList;
import java.util.HashSet;

public class Lahendaja {

    //Linnade vahelised kaugused
    public int[][] kaugused;

    //Sisaldab linnade nimesid
    //Saab kasutada nii rea kui tulba nime leidmiseks
    public String[] nimed;

    public Lahendaja(String fail) throws FileNotFoundException {
        loeKaugusedFailist(fail);
    }

    /**
     * Leiab lühima tee linnade 'algus' ja 'lõpp' vahel.
     * kasutatud ka https://en.wikipedia.org/wiki/Dijkstra%27s_algorithm
     * @param algus Alguslinn sõnena
     * @param lõpp  Lõpplinn sõnena
     * @param max   Maksimaalne kaugus, mida auto saab ühe tankimisega läbida
     *              (ülesande tekstis tähistatud kui x)
     * @return Vastuse isend, mis sisaldab leitud lühimat teed ja teepikkust
     * Kui vastust ei leidu (algus/lõpp ebasobiv, ei saa läbida sellise 'max'ga),
     * tagastada Vastus teepikkusega -1.
     */
    public Vastus leiaLühimTee(String algus, String lõpp, int max) {
        //Dijkstra algoritm on õpikus lehekülg 100.

        // jätan meelde linnad, kust on juba edasi liigutud
        HashSet<String> käidud = new HashSet<>();
        // sisaldab lühimaid teid algusest linna v
        int[] lühimadTeed = täidaArray(nimed.length);
        // lühimate teede järjekord
        Eelistusjärjekord<Tee> Q = new Kahendkuhi<Tee>(new ArrayList<>());

        Q.lisaElement(new Tee(algus));

        while (Q.getElementideArv() != 0) {
            // u on vaadeldav linn, ehk sealt otsitakse teid teistesse linnadesse
            Tee u = Q.popEsimene();
            if (käidud.contains(u.getViimane()))
                continue;

            käidud.add(u.getViimane());
            // kui tee u viimane linn on sama mis lõpp, siis tee u peab olema lühim tee lõppu
            if (u.getViimane().equals(lõpp))
                return new Vastus(u.getTeeAsArray(), u.getTeePikkus());

            for (int i = 0; i < nimed.length; i++) {
                int uJaVKaugus = kaugused[leiaLinnaIndeks(u.getViimane())][i];
                // kui linnade u ja v vaheline kaugus on liiga suur, siis võetakse järgmine
                if (uJaVKaugus > max)
                    continue;

                int uus_pikkus = u.getTeePikkus() + uJaVKaugus;
                if (uus_pikkus < lühimadTeed[i]){
                    lühimadTeed[i] = uus_pikkus;
                    Tee uus_Tee = new Tee(u);
                    uus_Tee.lisaSihtkoht(nimed[i], uJaVKaugus);
                    Q.lisaElement(uus_Tee);
                }
            }
        }

        return new Vastus(new String[]{algus, lõpp}, -1);
    }


    /**
     * koostab array, kus kõik elemendid on võimalikult suured
     * @param suurus, array elementide arv
     * @return array suurusega suurus
     */
    private int[] täidaArray(int suurus) {
        int[] maatriks = new int[suurus];
        for (int x = 0; x < suurus; x++)
            maatriks[x] = Integer.MAX_VALUE;
        return maatriks;
    }

    private int leiaLinnaIndeks(String linn) {
        for (int i = 0; i < nimed.length; i++)
            if (nimed[i].equals(linn))
                return i;
        return -1;
    }


    private void loeKaugusedFailist(String fail) throws FileNotFoundException {
        try (BufferedReader br = new BufferedReader(new FileReader(new File(fail)))) {
            nimed = br.readLine().split("\t");
            kaugused = new int[nimed.length][nimed.length];
            String in = br.readLine();
            int i = 0;
            while (in != null) {
                String[] reaJupid = in.split("\t");
                for (int j = 1; j < reaJupid.length; j++) {
                    if (!reaJupid[j].equals("")) {
                        kaugused[i][j - 1] = Integer.parseInt(reaJupid[j]);
                    } else {
                        kaugused[i][j - 1] = 0;
                    }
                }
                i++;
                in = br.readLine();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }


}