package homework.k7;

import java.util.ArrayList;
import java.util.List;

public class Tee implements Comparable<Tee> {
    private List<String> tee;
    private int teePikkus;

    public Tee(String algus) {
        this.tee = new ArrayList<>();
        this.tee.add(algus);
        this.teePikkus = 0;
    }

    public Tee(Tee tee){
        this.tee = new ArrayList<>();
        this.tee.addAll(tee.getTee());
        this.teePikkus = tee.getTeePikkus();
    }

    public void lisaSihtkoht(String linn, int teePikkus) {
        tee.add(linn);
        this.teePikkus += teePikkus;
    }

    public String getViimane(){
        return tee.get(tee.size() - 1);
    }

    public List<String> getTee() {
        return tee;
    }

    public int getTeePikkus() {
        return teePikkus;
    }

    public String[] getTeeAsArray() {
        String[] t = new String[tee.size()];
        for (int i = 0; i < tee.size(); i++)
            t[i] = tee.get(i);
        return t;
    }

    @Override
    public int compareTo(Tee o) {
        return Integer.compare(teePikkus, o.teePikkus);
    }
}
