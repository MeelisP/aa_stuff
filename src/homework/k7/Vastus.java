package homework.k7;

public class Vastus {

    private String[] tee;
    private int teepikkus;

    public Vastus(String[] tee,int teepikkus) {
        this.tee = tee;
        this.teepikkus = teepikkus;
    }

    public String toString() {
        StringBuilder sb = new StringBuilder();

        if(teepikkus != -1) {
            sb.append(teepikkus + ", ");

            for(int i = 0; i < tee.length; i++) {
                if(i == tee.length - 1) {
                    sb.append(tee[i]);
                } else {
                    sb.append(tee[i] + " > ");
                }
            }
        } else {
            sb.append("Linnade " + tee[0] + " ja " + tee[tee.length - 1]
                    + " vahel sobivat teed ei leidunud.");
        }
        return sb.toString();
    }
}
