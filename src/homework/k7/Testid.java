package homework.k7;

import org.junit.Test;

import java.io.FileNotFoundException;

import static org.junit.Assert.assertEquals;

public class Testid {

    private Lahendaja lahendaja = new Lahendaja("linnade_kaugused.txt");

    public Testid() throws FileNotFoundException {
    }

    @Test
    public void test01() {
        teeTest("Kuressaare","Narva",100,"433, Kuressaare > Kuivastu > Lihula > Saue > Tallinn > Rakvere > Kiviõli > Narva");
    }

    @Test
    public void test05() {
        teeTest("Valga","Tallinn",40,"Linnade Valga ja Tallinn vahel sobivat teed ei leidunud.");
    }

    public void teeTest(String algus,String lõpp,int piirang, String õige) {
        Vastus vastus = lahendaja.leiaLühimTee(algus,lõpp,piirang);
        String tulemus = vastus.toString();
        assertEquals(õige,tulemus);
    }
}
