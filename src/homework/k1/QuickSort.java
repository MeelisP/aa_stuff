package homework.k1;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * Algoritmid ja andmestruktuurid
 * 2017/2018 sügissemester
 * Kodutöö nr 1
 * Created by Meelis on 12/09/2017.
 */
//algoritmi tegin https://en.wikipedia.org/wiki/Quicksort pseoudokoodi põhjal.
public class QuickSort {

    public static void main(String[] args) {
        //tests();
        List<Integer> numbers = manageArgs(args);
        System.out.println("numbrid sorteerimata kujul: " + numbers);
        sort(numbers);
        System.out.println("numbrid sorteeritult" + numbers);

        //aja mõõtmine
        /*
        List<Integer> numbers = new ArrayList<>();
        for (int i = 100; i < 7500; i += 100) {
            System.out.println(i);
            numbers = getRandomList(i);
            measureDefaultSortTime(numbers);
        }
        */
    }

    private static void tests() {

        List<Integer> numbers = new ArrayList<>();
        /*
        System.out.println("Testing 0 element list");

        System.out.println("Unsorted: " + numbers.toString());
        sort(numbers);
        System.out.println("Sorted " + numbers);

        System.out.println("Testing 1 element list");
        numbers.add(5);
        System.out.println("Unsorted: " + numbers.toString());
        sort(numbers);
        System.out.println("Sorted " + numbers);
        */
        System.out.println("Testing 5 element list");
        numbers = getRandomList(10);
        System.out.println("Unsorted: " + numbers.toString());
        sort(numbers);
        System.out.println("Sorted " + numbers);

    }

    private static void measureSortTime (List<Integer> numbers) {
        long start = System.nanoTime() ;
        sort(numbers);
        long end = System.nanoTime() ;
        System.out.println("time: " + (end - start) / 1000000 + "ms");
    }

    private static void measureDefaultSortTime (List<Integer> numbers) {
        long start = System.nanoTime() ;
        Collections.sort(numbers);
        long end = System.nanoTime();
        System.out.println("time: " + (end - start) / 1000000  + "ms");
    }

    //kasuta seda meetodit suvalise listi genereerimiseks
    private static List<Integer> getRandomList(int size) {
        List<Integer> numbers = new ArrayList<>();
        for (int i = 0; i < size; i++) {
            numbers.add((int) (Math.random()*100000));
        }
        return numbers;
    }

    //teeb käsurea muutujad täisarvudeks.
    private static List<Integer> manageArgs(String[] args) {
        List<Integer> numbers = new ArrayList<>();
        for (String arg : args) {
            numbers.add(Integer.parseInt(arg));
        }
        return numbers;
    }

    private static void sort(List<Integer> numbers) {
         sort(numbers, 0, numbers.size() - 1);
    }

    private static void sort(List<Integer> numbers, int lowerIndex, int higherIndex) {

        if (lowerIndex < higherIndex) {
            int p = partition(numbers, lowerIndex, higherIndex);
            sort(numbers, lowerIndex, p - 1);
            sort(numbers, p + 1, higherIndex);
        }
    }

    private static int partition(List<Integer> numbers, int lowerIndex, int higherIndex) {
        int pivot = numbers.get(higherIndex);
        int swapIndex = lowerIndex - 1;
        for (int i = lowerIndex; i < higherIndex; i++) {
            int currentNumber = numbers.get(i);
            if (currentNumber > pivot) {
                swapIndex++;
                //swap

                int swapableNumber = numbers.get(swapIndex);
                numbers.set(swapIndex, currentNumber);
                numbers.set(i, swapableNumber);
            }
        }
        int swapableNumber = numbers.get(swapIndex + 1);
        if (pivot > swapableNumber) {
            //swap
            numbers.set(swapIndex + 1, pivot);
            numbers.set(higherIndex, swapableNumber);
        }
        return swapIndex + 1;
    }
}
