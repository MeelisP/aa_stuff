package homework.k1;

import java.util.ArrayList;
import java.util.List;

/**
 * Algoritmid ja andmestruktuurid
 * 2017/2018 sügissemester
 * Kodutöö nr 1
 * Created by Meelis on 11/09/2017.
 */

//kasutasin https://en.wikipedia.org/wiki/Bubble_sort pseudokoodi.
public class BubbleSort {

    public static void main(String[] args) {
        //tests();
        List<Integer> numbers = manageArgs(args);
        System.out.println("numbrid sorteerimata kujul: " + numbers);
        sort(numbers);
        System.out.println("numbrid sorteeritult" + numbers);

        //aja mõõtmine
        /*
        for (int i = 100; i < 7500; i += 100) {
            numbers = getRandomList(i);
            measureSortTime(numbers);
        }
        */
    }

    //äärejuhtude testimiseks
    private static void tests() {
        System.out.println("Testing 0 element list");
        List<Integer> numbers = new ArrayList<>();
        System.out.println("Unsorted: " + numbers.toString());
        System.out.println("Sorted " + sort(numbers).toString());

        System.out.println("Testing 1 element list");
        numbers.add(5);
        System.out.println("Unsorted: " + numbers.toString());
        System.out.println("Sorted " + sort(numbers).toString());

        System.out.println("Testing 5 element list");
        numbers = getRandomList(5);
        System.out.println("Unsorted: " + numbers.toString());
        System.out.println("Sorted " + sort(numbers).toString());


    }

    private static void measureSortTime (List<Integer> numbers) {
        long start = System.currentTimeMillis();
        sort(numbers);
        long end = System.currentTimeMillis();
        System.out.println("time " + (end - start) + "ms");
    }

    //kasuta seda meetodit suvalise listi genereerimiseks
    private static List<Integer> getRandomList(int size) {
        List<Integer> numbers = new ArrayList<>();
        for (int i = 0; i < size; i++) {
            numbers.add((int) (Math.random()*100000));
        }
        return numbers;
    }

    //teeb käsurea muutujad täisarvudeks.
    private static List<Integer> manageArgs(String[] args) {
        List<Integer> numbers = new ArrayList<>();
        for (String arg : args) {
            numbers.add(Integer.parseInt(arg));
        }
        return numbers;
    }

    private static List<Integer> sort(List<Integer> numbers) {

        while (true) {
            boolean swapped = false;
            for (int i = 0; i < numbers.size() - 1; i++) {
                int numberBeforeCheckLine = numbers.get(i);
                int numberAfterCheckLine = numbers.get(i + 1);
                if (numberBeforeCheckLine < numberAfterCheckLine) {
                    numbers.set(i, numberAfterCheckLine);
                    numbers.set(i + 1, numberBeforeCheckLine);
                    swapped = true;
                }
            }
            if (!swapped)
                break;
        }
        return numbers;
    }
}
