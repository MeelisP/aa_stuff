package homework.k3;

import com.sun.jmx.remote.internal.ArrayQueue;

import java.io.File;
import java.util.*;

/* Algoritmid ja andmestruktuurid
* 2017/2018 sügissemester
* Kodutöö nr 3
* @author Meelis Perli
*/
public class kodutöö3a {

    public static void main(String[] args) {
        //puu koostamiseks anna käsurea argumendiks faili path
        koostaFailiPuu(new File(args[0]));
    }
    /**
     * Kuvab kausta koos taandega
     * @param kaust, kuvatav kaust
     * @param taane, number, mis tagab piisava taande
     */
    private static void kuvaKaust(File kaust, int taane) {

        for (int i = 0; i < taane; i++) {
            System.out.print("    ");
        }
        if (kaust.isDirectory())
            System.out.println("[" + kaust.getName() + "]");
        else if (kaust.length() / 1000 <= 500) {
            System.out.println(kaust.getName() + " (" + kaust.length() / 1000 + " KB)");
        }

    }

    /**
     *  Meetod, mis koostab failipuu.
     * @param juurKaust <- kaust, millest alustatakse failipuu tegemist
     */
    public static void koostaFailiPuu(File juurKaust) {
        Stack<TaaneKoosFailiga> failid = new Stack<>();
        failid.add(new TaaneKoosFailiga(0, juurKaust));

        //while loopis võetakse järjest stacki elemente, kuvatakse see ja kontrollitakse,
        // et kas tegu on kaustaga. Kui on, siis lisatakse kõik seal kaustas olevad failid stacki peale.
        while (failid.size() > 0) {
            TaaneKoosFailiga tkf = failid.pop();
            kuvaKaust(tkf.getFail(), tkf.getTaane());

            if (tkf.getFail().isDirectory()) {
                int taane = tkf.getTaane();
                for (File fail : tkf.getFail().listFiles()) {
                    failid.add(new TaaneKoosFailiga(taane + 1, fail));
                }
            }
        }
    }

    //Lihtsalt mingi klass, mis hoiab faili koos taande numbriga
    public static class TaaneKoosFailiga {
        private int taane;
        private File fail;

        public TaaneKoosFailiga(int taane, File fail) {
            this.taane = taane;
            this.fail = fail;
        }

        public int getTaane() {
            return taane;
        }

        public void setTaane(int taane) {
            this.taane = taane;
        }

        public File getFail() {
            return fail;
        }

        public void setFail(File fail) {
            this.fail = fail;
        }
    }
}
