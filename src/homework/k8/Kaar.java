package homework.k8;

public class Kaar implements Comparable<Kaar> {

	private Tipp algus;
	private Tipp lopp;
	private int kaal;

	public Kaar(Tipp algus, Tipp lopp) {
		this(algus, lopp, 1);
	}

	public Kaar(Tipp algus, Tipp lopp, int kaal) {
		this.algus = algus;
		this.lopp = lopp;
		this.kaal = kaal;
	}

	public Tipp getAlgus() {
		return algus;
	}

	public Tipp getLopp() {
		return lopp;
	}

	public int getKaal() {
		return kaal;
	}

	public String toString() {
		return algus + " -> " + lopp;
	}

	@Override
	public int compareTo(Kaar k) {
		return Integer.compare(kaal, k.kaal);
	}
}
