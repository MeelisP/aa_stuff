package homework.k8;

import java.util.ArrayList;
import java.util.List;

public class Kahendkuhi<T extends Comparable<T>> implements Eelistusjärjekord<T> {

    private List<T> kuhi = new ArrayList<>();

    public Kahendkuhi(List<T> kuhi) {
        this.kuhi = kuhi;
    }

    public void lisaElement(T element){
        int uueIndex = kuhi.size();
        kuhi.add(element);
        viiElementMullinaÜles(uueIndex);
    }

    @Override
    public T popEsimene() {
        T viimane = kuhi.get(kuhi.size() - 1);
        T esimene = kuhi.get(0);
        kuhi.set(0, viimane);
        kuhi.remove(kuhi.size() - 1);
        viiElementMullinaAlla(0);
        return esimene;
    }

    @Override
    public int getElementideArv() {
        return kuhi.size();
    }

    @Override
    public void prindi() {
        for (T t : kuhi) {
            System.out.println(t);
        }
    }

    public int getParemaAlluvaIndex(int k) {
        if (kuhi.size() - 1 >= 2 * k + 2)
            return 2 * k + 2;
        return -1;
    }

    public int getVasakuAlluvaIndex(int k) {
        if (kuhi.size() - 1 >= 2 * k + 1)
            return 2 * k + 1;
        return -1;
    }

    public int getVanemaIndex(int k) {
        if (kuhi.size() - 1 < k || k == 0)
            return -1;
        return (int) Math.floor((k - 1) / 2);
    }

    //kui element kohal k on väiksem oma ülemusest, siis nad vahetavad kohad
    public void viiElementMullinaÜles(int k) {
        int vanemaIndex = getVanemaIndex(k);
        if (vanemaIndex == -1)
            return;

        if (kuhi.get(k).compareTo(kuhi.get(vanemaIndex)) < 0) {
            vahetaElementideKohad(k, vanemaIndex);
            viiElementMullinaÜles(vanemaIndex);
        }
    }

    //kui element kohal k on suurem vähemalt ühest alluvast, siis vahetab vähimaga koha.
    public void viiElementMullinaAlla(int k) {
        if (kuhi.size() - 1 < k)
            return;
        int vasakuIndeks = getVasakuAlluvaIndex(k);
        int paremaIndeks = getParemaAlluvaIndex(k);
        int vahetatavaIndeks = -1;
        T vahetatav = null;

        if (vasakuIndeks != -1) {
            vahetatavaIndeks = vasakuIndeks;
            vahetatav = kuhi.get(vasakuIndeks);
        } else
            return; // kui vasakut haru pole, on kindlasti tegemist lehega

        if (paremaIndeks != -1){
            T parem = kuhi.get(paremaIndeks);
            if (parem.compareTo(vahetatav) < 0) {
                vahetatav = parem;
                vahetatavaIndeks = paremaIndeks;
            }
        }

        if (kuhi.get(k).compareTo(vahetatav) > 0) {
            vahetaElementideKohad(k, vahetatavaIndeks);
            viiElementMullinaAlla(vahetatavaIndeks);
        }

    }

    private void vahetaElementideKohad(int k1, int k2) {
        T elementA = kuhi.get(k1);
        T elementB = kuhi.get(k2);
        kuhi.set(k1, elementB);
        kuhi.set(k2, elementA);
    }

    public String toString() {
        return toString(0);
    }

    private String toString(int k) {
        String puu = "";
        if (getParemaAlluvaIndex(k) != -1)
            puu += toString(getParemaAlluvaIndex(k));
        puu += toStringRow(k);
        if (getVasakuAlluvaIndex(k) != -1)
            puu += toString(getVasakuAlluvaIndex(k));
        return puu;
    }

    private String toStringRow(int k) {
        int suurimKõrgus = (int) Math.floor(Math.log(kuhi.size()) / Math.log(2));
        int kKõrgus = (int) Math.floor(Math.log(k + 1) / Math.log(2));
        String rida = "";
        for (int i = 0; i < kKõrgus; i++) {
            rida += "-";
        }
        rida += kuhi.get(k);
        for (int i = 0; i < suurimKõrgus - kKõrgus; i++) {
            rida += "-";
        }
        rida += "\n";
        return rida;
    }


}