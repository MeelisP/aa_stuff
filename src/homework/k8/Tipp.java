package homework.k8;

import java.util.HashSet;
import java.util.Set;

public class Tipp{

	private String nimi;
	
	private Set<Kaar> valjuvadKaared;
	
	private Set<Kaar> sisenevadKaared;
	
	public Tipp(String nimi) {
		this.nimi = nimi;
		
		valjuvadKaared = new HashSet<Kaar>();
		sisenevadKaared = new HashSet<Kaar>();
	}

	public String getNimi() {
		return nimi;
	}
	
	public void lisaValjuvKaar(Kaar kaar) {
		valjuvadKaared.add(kaar);
	}
	
	public Set<Kaar> getValjuvadKaared() {
		return valjuvadKaared;
	}
	
	public void lisaSisenevKaar(Kaar kaar) {
		sisenevadKaared.add(kaar);
	}

	public Set<Kaar> getSisenevadKaared() {
		return sisenevadKaared;
	}

	public String toString() {
		return nimi;
	}

}
