package homework.k8;

import java.io.*;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;

public class Graaf {
	
    private List<Tipp> tipud;
    private List<Kaar> kaared;

    public Graaf() {
        this.tipud = new ArrayList<Tipp>();
        this.kaared = new ArrayList<Kaar>();
    }

    public Graaf(List<Tipp> V, List<Kaar> E) {
        this.tipud = V;
        this.kaared = E;
    }

    public void lisaTipp(Tipp t){
        tipud.add(t);
    }

    public void lisaKaar(Tipp algus, Tipp lopp, int kaal){
        //korrektsuse tagamine (pole hea kiiruse ja efektiivsuse jaoks, aga igaks juhuks)
        if (!tipud.contains(algus)) tipud.add(algus);
        if (!tipud.contains(lopp)) tipud.add(lopp);

        Kaar kaar = new Kaar(algus,lopp,kaal);
        algus.lisaValjuvKaar(kaar);
        lopp.lisaSisenevKaar(kaar);

        kaared.add(kaar);
    }

    public void lisaKaar(Tipp algus, Tipp lopp){
        lisaKaar(algus, lopp, 1);
    }

    public List<Tipp> getTipud() {
        return tipud;
    }
    public List<Kaar> getKaared() {
        return kaared;
    }

    /**
     * leiab graafi toesPuu Primi algoritmiga
     * @return
     */
    public List<Kaar> leiaToespuuPrimiAlgoritmiga() {
        List<Kaar> toesPuuKaared = new ArrayList<>();
        HashSet<Tipp> toesPuuTipud = new HashSet<>();
        Eelistusjärjekord<Kaar> valjuvateKaarteJarjekord = new Kahendkuhi<Kaar>(new ArrayList<>());
        toesPuuTipud.add(tipud.get(0));
        for (Kaar kaar : tipud.get(0).getValjuvadKaared())
            valjuvateKaarteJarjekord.lisaElement(kaar);

        while (valjuvateKaarteJarjekord.getElementideArv() != 0) {
            Kaar lyhimKaar = valjuvateKaarteJarjekord.popEsimene();
            // kui toesPuus on juba vaadeldava kaare lõpp, siis tekib tsükkel.
            if (toesPuuTipud.contains(lyhimKaar.getLopp()))
                continue;

            toesPuuKaared.add(lyhimKaar);
            toesPuuTipud.add(lyhimKaar.getLopp());

            //Pole mõtet edasi vaadata, kui toespuu sisaldab sama palju tippe võrreldes esialgse graafiga.
            if (toesPuuTipud.size() == tipud.size())
                break;

            for (Kaar kaar : lyhimKaar.getLopp().getValjuvadKaared()) {
                valjuvateKaarteJarjekord.lisaElement(kaar);
            }
        }
        // toesPuus peab olema sama palju tippe, nagu on esialgses graafis
        if (toesPuuTipud.size() == tipud.size())
            return toesPuuKaared;

        return new ArrayList<>();
    }

    public String toString(){
        String s = "";
        s = s + "digraph G {\n";
        for(Kaar kaar : kaared){
            s = s + "    " + kaar
                           + "[label = \" " + kaar.getKaal() + "\"]\n";
        }
        s = s + "}";
        return s;
    }

    /**
     * loeb failist graafi
     * @param fail, naabrusmaatriksit sisaldav fail
     */
    public void votaGraafFailist(String fail) {
        try (BufferedReader br = new BufferedReader(new FileReader(new File(fail)))) {
            String[] tipuNimed = br.readLine().split("\t");
            int[][] kaalud = new int[tipuNimed.length][tipuNimed.length];
            String in = br.readLine();
            int i = 0;
            while (in != null) {
                String[] reaJupid = in.split("\t");
                for (int j = 1; j < reaJupid.length; j++) {
                    if (!reaJupid[j].equals("")) {
                        kaalud[i][j - 1] = Integer.parseInt(reaJupid[j]);
                    } else {
                        kaalud[i][j - 1] = 0;
                    }
                }
                i++;
                in = br.readLine();
            }
            for (String s : tipuNimed) {
                lisaTipp(new Tipp(s));
            }
            for (int x = 0; x < tipuNimed.length; x++)
                for (int y = 0; y < tipuNimed.length; y++)
                    lisaKaar(tipud.get(x), tipud.get(y), kaalud[x][y]);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
