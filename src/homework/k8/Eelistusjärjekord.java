package homework.k8;

public interface Eelistusjärjekord<T extends Comparable<T>> {
	
	void lisaElement(T element);

	/**
	 * eemaldab 1. elemendi ja paneb uue selle asemele
	 * @return 1. element
	 */
	T popEsimene();
	
	int getElementideArv();

	void prindi();
}
