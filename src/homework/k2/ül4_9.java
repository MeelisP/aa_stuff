package homework.k2;

import java.util.ArrayList;
import java.util.List;

/* Algoritmid ja andmestruktuurid
* 2017/2018 sügissemester
* Kodutöö nr 2
* @author Meelis Perli
*/

public class ül4_9 {

    public static void main(String[] args) {
        List<String> rongid = genereeriRonge(Integer.parseInt(args[0]));
        kuvaTulemus(rongid);
    }

    /**
     *  kuvab kõik vagunite järjestused ja nende arvu
     * @param rongid list, mis sisaldab vagunite järjestusi
     */
    private static void kuvaTulemus(List<String> rongid) {
        System.out.println("Kõik võimalikud rongid:");
        for (String s : rongid) {
            System.out.println(s);
        }
        System.out.println("Neid on: " + rongid.size());
    }

    /**
     *
     * @param pikkus rongi pikkus
     * @return kõik võimalikud vagunite järjestused
     */
    public static List<String> genereeriRonge(int pikkus) {
        return genereeriRonge(pikkus, "", new ArrayList<>(), "");
    }

    /*
     * Reeglid
     * 1. C-osa ei või vahetult järgneda B-osale ega B-osa C-osale;
     * 2. D-osa ei või vahetult järgneda A-osale;
     * 3. järjestikused ühte ja sama tüüpi osad pole lubatud
     *
     * @param pikkus rongi pikkus
     * @param eelmineVagun jätab meelde eelmise vaguni, et reegleid saaks rakendada, sisendina panna tühi string
     * @param rongid hoiab ronge, sisendina panna tühi list
     * @param rong jätab meelde, mis vagunid on, sisendiks panna tühi string
     * @return kõik võimalikud vagunite järjestused
     */
    private static List<String> genereeriRonge(int pikkus, String eelmineVagun, List<String> rongid, String rong) {

        if (pikkus == 0) {
            //pean seda kontrollima, sest muidu ei tagasta tühja listi, kui rongi pikkus on 0
            if (!rong.equals(""))
                rongid.add(rong);
            return rongid;
        }

        if (!eelmineVagun.equals("A"))
            genereeriRonge(pikkus - 1, "A", rongid, rong + "A");
        if (!eelmineVagun.equals("B") && !eelmineVagun.equals("C"))
            genereeriRonge(pikkus - 1, "B", rongid, rong + "B");
        if (!eelmineVagun.equals("C") && !eelmineVagun.equals("B"))
            genereeriRonge(pikkus - 1, "C", rongid, rong + "C");
        if (!eelmineVagun.equals("D") && !eelmineVagun.equals("A"))
            genereeriRonge(pikkus - 1, "D", rongid, rong + "D");

        return rongid;
    }
}
