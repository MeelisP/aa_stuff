package homework.k2;

import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

public class ül4_9testid {

    @Test
    public void TestRongidPikkusega0(){
        List<String> rongid = ül4_9.genereeriRonge(0);
        assertTrue(rongid.isEmpty());
        List<String> rongid2 = new ArrayList<>();
        assertEquals(rongid2, rongid);
    }

    @Test
    public void TestRongidPikkusega1(){
        List<String> rongid = ül4_9.genereeriRonge(1);
        assertEquals(4, rongid.size());
        List<String> rongid2 = new ArrayList<>();
        rongid2.add("A");
        rongid2.add("B");
        rongid2.add("C");
        rongid2.add("D");
        assertEquals(rongid2, rongid);
    }

    //C-osa ei või vahetult järgneda B-osale ega B-osa C-osale;
    @Test
    public void TestRongiReegel1(){
        List<String> rongid = ül4_9.genereeriRonge(4);
        for (String s : rongid) {
            assertFalse(s.contains("BC"));
            assertFalse(s.contains("CB"));
        }
    }

    //D-osa ei või vahetult järgneda A-osale;
    @Test
    public void TestRongiReegel2(){
        List<String> rongid = ül4_9.genereeriRonge(4);
        for (String s : rongid) {
            assertFalse(s.contains("AD"));
        }
    }

    //järjestikused ühte ja sama tüüpi osad pole lubatud
    @Test
    public void TestRongiReegel3(){
        List<String> rongid = ül4_9.genereeriRonge(4);
        for (String s : rongid) {
            assertFalse(s.contains("AA"));
            assertFalse(s.contains("BB"));
            assertFalse(s.contains("CC"));
            assertFalse(s.contains("DD"));
        }
    }


}
