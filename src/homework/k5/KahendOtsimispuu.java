package homework.k5;

public interface KahendOtsimispuu<T extends Comparable> {

    /**
     * Lisab kirje kahendpuusse
     * @param kirje - lisatav kirje
     * @return - viide (alam)puu juurele
     * st viide iseendale või tipule, mis asendab antud tippu puu struktuuris.
     */
    public KahendOtsimispuu<T> lisaKirje(T kirje);

    /**
     * Eemaldab kirje kahendotsimispuust
     * @param kirje - lisatav kirje
     * @return - viide (alam)puu juurele
     * st viide iseendale või tipule, mis asendab antud tippu puu struktuuris.
     */
    public KahendOtsimispuu<T> eemaldaKirje(T kirje);

    public String sulgEsitus();

}