package homework.k5;

import homework.k5.AVLKahendOtsimispuu;
import homework.k5.KahendOtsimispuu;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.Stack;

public class Tipp<T extends Comparable> implements KahendOtsimispuu<T>, AVLKahendOtsimispuu<T> {

    protected T info;
    protected Tipp<T> vasakAlluv;
    protected Tipp<T> paremAlluv;

    public Tipp(T info) {
        this(info, null, null);
}

    public Tipp(T info, Tipp vasakAlluv, Tipp paremAlluv) {
        this.info = info;
        this.vasakAlluv = vasakAlluv;
        this.paremAlluv = paremAlluv;
    }

    @Override
    public AVLKahendOtsimispuu<T> lisaKirjeAVL(T kirje) {

        this.lisaKirje(kirje);
        return this.tasakaalusta(new Tipp<T>(kirje, null, null));
    }

    @Override
    public AVLKahendOtsimispuu<T> eemaldaKirjeAVL(T kirje) {
        Tipp<T> juur = this.otsiKirjeÜlemTipp(kirje);
        this.eemaldaKirje(kirje);

        if (juur == null)
            return this;

        if (juur.paremAlluv == null && juur.vasakAlluv == null)
            return this.tasakaalusta(juur);

        if (juur.paremAlluv != null) {
            if (juur.paremAlluv.paremAlluv != null)
                this.tasakaalusta(juur.paremAlluv.paremAlluv);
            if (juur.paremAlluv.vasakAlluv != null)
                this.tasakaalusta(juur.paremAlluv.vasakAlluv);
        }
        if (juur.vasakAlluv != null) {
            if (juur.vasakAlluv.paremAlluv != null)
                this.tasakaalusta(juur.vasakAlluv.paremAlluv);
            if (juur.vasakAlluv.vasakAlluv != null)
                this.tasakaalusta(juur.vasakAlluv.vasakAlluv);
        }
        return this;
    }

    @Override
    public KahendOtsimispuu<T> lisaKirje(T kirje) {

        if (kirjeOnVäiksemInfost(kirje)) {
            if (this.vasakAlluv == null) {
                this.vasakAlluv = new Tipp<>(kirje);
                return this;
            }
            this.vasakAlluv.lisaKirje(kirje);
        } else {
            if (this.paremAlluv == null) {
                this.paremAlluv = new Tipp<>(kirje);
                return this;
            }
            this.paremAlluv.lisaKirje(kirje);
        }
        return this;
    }

    @Override
    public KahendOtsimispuu<T> eemaldaKirje(T kirje) {
        Tipp<T> kustutatavTipp = otsiTipp(kirje);

        if (kustutatavTipp == null)
            return this;

        Tipp<T> asendavTipp = kustutatavTipp.leiaKeskjärjestusesJärgmineTipp();

        if (asendavTipp != null) {
            T info = asendavTipp.info;

            KahendOtsimispuu<T> alamPuu = this.otsiKirjeÜlemTipp(asendavTipp.info);

            alamPuu.eemaldaKirje(info);
            kustutatavTipp.info = info;
        } else
            this.kustutaKirje(kirje);

        return this;
    }

    /**
     * tasakaalustab avl puu
     *
     * @param tipp, tipp mille juurest alustatakse tasakaalustamist
     * @return puu peale tasakaalustamist
     */
    private Tipp<T> tasakaalusta(Tipp<T> tipp) {

        Tipp<T> vanem = this.otsiTipuÜlemTipp(tipp);

        Tipp<T> uusVanem;

        if (vanem == null)
            return this;

        int[] kõrgused = vanem.leiaHarudeKõrgused();

        // tasakaalustamine

        // parem haru on sügavam
        if (kõrgused[0] - kõrgused[1] < -1) {
            kõrgused = vanem.paremAlluv.leiaHarudeKõrgused();

            if (kõrgused[0] - kõrgused[1] > 0)
                uusVanem = vanem.paremVasakPööre();
            else
                uusVanem = vanem.vasakPööre();

            // vasak haru on sügavam
        } else if (kõrgused[0] - kõrgused[1] > 1) {
            kõrgused = vanem.vasakAlluv.leiaHarudeKõrgused();

            if (kõrgused[0] - kõrgused[1] < 0)
                uusVanem = vanem.vasakParemPööre();
            else
                uusVanem = vanem.paremPööre();

        } else {
            return this.tasakaalusta(vanem);
        }
        //tulemuse tagastamine
        Tipp<T> vanemaVanem = this.otsiTipuÜlemTipp(vanem);
        if (vanemaVanem == null) {
            this.info = uusVanem.info;
            this.paremAlluv = uusVanem.paremAlluv;
            this.vasakAlluv = uusVanem.vasakAlluv;
            return uusVanem;
        } else if (vanemaVanem.paremAlluv != null && vanemaVanem.paremAlluv.info == vanem.info)
            vanemaVanem.paremAlluv = uusVanem;
        else
            vanemaVanem.vasakAlluv = uusVanem;

        return this.tasakaalusta(uusVanem);

    }

    public boolean ul1(Tipp<T> ülem) {
        if (ülem != null && ülem.info == this.info) {
            if (this.paremAlluv != null && this.paremAlluv.info == this.info)
                return true;
            if (this.vasakAlluv != null && this.vasakAlluv.info == this.info)
                return true;
        }

        boolean vp = false;
        boolean pp = false;

        if (this.paremAlluv != null)
            pp = paremAlluv.ul1(this);
        if (this.vasakAlluv != null)
            vp = vasakAlluv.ul1(this);

        return pp || vp;
    }

    public int ul2() {
        if (this.vasakAlluv == null && this.paremAlluv == null)
            return 1;

        int s = 0;
        if ((this.vasakAlluv != null && this.kirjeOnVäiksemInfost(this.vasakAlluv.info) || this.vasakAlluv == null) &&
                (this.paremAlluv != null && this.kirjeOnVäiksemInfost(this.paremAlluv.info) || this.paremAlluv == null))
            s = 1;
        int vs = 0;
        int ps = 0;
        if (this.vasakAlluv != null) {
            vs = this.vasakAlluv.ul2();
        }
        if (this.paremAlluv != null) {
            ps = this.paremAlluv.ul2();
        }
        return s + vs + ps;
    }

    public int ul3(int n) {
        //System.out.println(this.info + "-" + n); //esi
        //n += 1;
        if (this.vasakAlluv != null) {
            n = this.vasakAlluv.ul3(n);
        }
        System.out.println(this.info + "-" + n);// kesk
        n += 1;
        if (this.paremAlluv != null) {
            n = this.paremAlluv.ul3(n);
        }
        //System.out.println(this.info + "-" + n); //lõppjärjestus
        //n += 1;
        return n;
    }

    /**
     * leiav tipu harude maksimaalsed kõrgused
     *
     * @return tagastab massiivi, kus [0] on vasaku haru kõrgus ja [1] on parema haru kõrgus.
     */
    private int[] leiaHarudeKõrgused() {
        int pKõrgus = 0;
        int vKõrgus = 0;

        if (this.paremAlluv != null)
            pKõrgus = this.paremAlluv.kõrgus() + 1;
        if (this.vasakAlluv != null)
            vKõrgus = this.vasakAlluv.kõrgus() + 1;
        return new int[]{vKõrgus, pKõrgus};
    }

    /**
     * pöörab antud puud paremale
     *
     * @return paremale pööratud puu
     */
    public Tipp<T> paremPööre() {
        Tipp<T> vvHaru;
        Tipp<T> vpHaru;
        Tipp<T> pHaru = this.paremAlluv;

        if (this.vasakAlluv != null) {
            vvHaru = this.vasakAlluv.vasakAlluv;
            vpHaru = this.vasakAlluv.paremAlluv;
            return new Tipp<T>(this.vasakAlluv.info, vvHaru, new Tipp<T>(this.info, vpHaru, pHaru));
        }

        return new Tipp<T>(this.info, null, pHaru);
    }

    /**
     * pöörab antud puud vasakule
     *
     * @return vasakule pööratud puu
     */
    public Tipp<T> vasakPööre() {
        Tipp<T> ppHaru;
        Tipp<T> pvHaru;
        Tipp<T> vHaru = this.vasakAlluv;

        if (this.paremAlluv != null) {
            ppHaru = this.paremAlluv.paremAlluv;
            pvHaru = this.paremAlluv.vasakAlluv;
            return new Tipp<T>(this.paremAlluv.info, new Tipp<T>(this.info, vHaru, pvHaru), ppHaru);
        }

        return new Tipp<T>(this.info, vHaru, null);
    }

    /**
     * pöörab antud puud paremale ja siis vasakule
     *
     * @return puu peale paremVasakPööret
     */
    public Tipp<T> paremVasakPööre() {
        Tipp<T> pööratudPuu = this;
        pööratudPuu.paremAlluv = pööratudPuu.paremAlluv.paremPööre();
        return pööratudPuu.vasakPööre();
    }

    /**
     * pöörab antud puud paremale
     *
     * @return puu peale vasakParempööret
     */
    public Tipp<T> vasakParemPööre() {
        Tipp<T> pööratudPuu = this;
        pööratudPuu.vasakAlluv = pööratudPuu.vasakAlluv.vasakPööre();
        return pööratudPuu.paremPööre();
    }

    /**
     * @param kirje, element, mis eemaldatakse puust
     * @return puu, milles pole antud kirjet
     */
    private Tipp<T> kustutaKirje(T kirje) {
        Tipp<T> vanemTipp = this.otsiKirjeÜlemTipp(kirje);

        if (vanemTipp == null)
            return this;

        if (vanemTipp.vasakAlluv != null && kirje == vanemTipp.vasakAlluv.info)
            vanemTipp.setVasakAlluv(null);
        else
            vanemTipp.setParemAlluv(null);
        return this;
    }

    /**
     * leiab keskjärjestusesTipu
     *
     * @return tagastab tipu, null kui ei leidu
     */
    private Tipp<T> leiaKeskjärjestusesJärgmineTipp() {
        Tipp<T> tipp;

        if (this.paremAlluv == null && this.vasakAlluv != null) {
            tipp = this.vasakAlluv;

            while (tipp.paremAlluv != null)
                tipp = tipp.paremAlluv;

            return tipp;
        }

        if (this.paremAlluv != null)
            tipp = this.paremAlluv;
        else
            return null;

        while (tipp.vasakAlluv != null)
            tipp = tipp.vasakAlluv;

        return tipp;
    }

    /**
     * @param kirje, element mille ülem tippu tahetakse leida
     * @return kirjele vastava juure, null kui ei leidu
     */
    private Tipp<T> otsiKirjeÜlemTipp(T kirje) {

        if (this.paremAlluv != null && kirje.equals(this.paremAlluv.info) || this.vasakAlluv != null && kirje.equals(this.vasakAlluv.info))
            return this;


        if (kirjeOnVäiksemInfost(kirje)) {
            if (vasakAlluv != null)
                return vasakAlluv.otsiKirjeÜlemTipp(kirje);
        } else {
            if (paremAlluv != null)
                return paremAlluv.otsiKirjeÜlemTipp(kirje);
        }

        return null;
    }

    /**
     * @param tipp, element mille ülem tippu tahetakse leida
     * @return tipule vastav juur, null kui ei leidu
     */
    private Tipp<T> otsiTipuÜlemTipp(Tipp<T> tipp) {

        if (this.paremAlluv != null && this.paremAlluv.equals(tipp) || this.vasakAlluv != null && this.vasakAlluv.equals(tipp))
            return this;

        if (kirjeOnVäiksemInfost(tipp.info)) {
            if (vasakAlluv != null)
                return vasakAlluv.otsiTipuÜlemTipp(tipp);
        } else {
            if (paremAlluv != null)
                return paremAlluv.otsiTipuÜlemTipp(tipp);
        }

        return null;
    }

    /**
     * @param kirje, element mida sisaldavat tippu tahetakse leida
     * @return, kirjele vastav tipp, null kui ei leidu
     */
    private Tipp<T> otsiTipp(T kirje) {
        if (kirje.equals(info))
            return this;

        if (kirjeOnVäiksemInfost(kirje)) {
            if (vasakAlluv != null)
                return vasakAlluv.otsiTipp(kirje);
        } else {
            if (paremAlluv != null)
                return paremAlluv.otsiTipp(kirje);
        }

        return null;
    }

    /**
     * tagastab kas kirje on väiksem tipu infost
     *
     * @param kirje, võrreldav element
     * @return kas kirje on väiksem infost
     */
    private boolean kirjeOnVäiksemInfost(T kirje) {
        return kirje.compareTo(info) < 0;
    }

    public T getInfo() {
        return info;
    }

    /**
     * leiab puu kõrguse
     *
     * @return puu kõrgus
     */
    public int kõrgus() {
        int vasakul = this.vasakAlluv == null ? -1 : this.vasakAlluv.kõrgus();
        int paremal = this.paremAlluv == null ? -1 : this.paremAlluv.kõrgus();
        return 1 + Math.max(vasakul, paremal);
    }

    /**
     * leiab tippude arvu
     *
     * @return tippude arv
     */
    public int tippudeArv() {
        //[Ülesanne 2]
        int vasakul = this.vasakAlluv == null ? 0 : this.vasakAlluv.tippudeArv();
        int paremal = this.paremAlluv == null ? 0 : this.paremAlluv.tippudeArv();
        return vasakul + paremal + 1;
    }

    public String sulgEsitus() {
        return sulgEsitusI(this);
    }

    protected static String sulgEsitusI(Tipp t) {
        if (t == null) {
            return "()";
        }
        return t.getInfo().toString()
                + "(" + sulgEsitusI(t.getVasakAlluv())
                + sulgEsitusI(t.getParemAlluv()) + ")";
    }

    public static Tipp<Integer> täidaArvudega(Tipp<Integer> struktuuripuu) {
        return täidaArvudega(struktuuripuu, 1, struktuuripuu.tippudeArv());
    }

    public static Tipp<Integer> täidaArvudega(Tipp<Integer> struktuuripuu, int algus, int lõpp) {
        //TODO: Implementeeri mind! [Ülesanne 3]

        Tipp<Integer> täidetudVasak;
        Tipp<Integer> täidetudParem;
        int vasakTA;
        if (struktuuripuu.vasakAlluv == null) {
            täidetudVasak = null;
        } else {
            vasakTA = struktuuripuu.getVasakAlluv().tippudeArv();
            täidetudVasak = täidaArvudega(struktuuripuu.getVasakAlluv(), algus, algus + vasakTA);
        }

        if (struktuuripuu.paremAlluv == null) {
            täidetudParem = null;
        } else {
            int paremTA = struktuuripuu.getParemAlluv().tippudeArv();
            täidetudParem = täidaArvudega(struktuuripuu.getParemAlluv(), algus + paremTA + 1, lõpp);
        }

        return new Tipp<Integer>(algus, täidetudVasak, täidetudParem);
    }

    public List<T> eesjärjestus() {
        List<T> tulemus = new ArrayList<>();
        Stack<Tipp<T>> s = new Stack<>();
        s.add(this);
        while (!s.isEmpty()) {
            Tipp<T> vaadeldav = s.pop();
            if (vaadeldav != null) {
                tulemus.add(vaadeldav.getInfo());
                s.push(vaadeldav.getParemAlluv());
                s.push(vaadeldav.getVasakAlluv());
            }
        }
        return tulemus;
    }

    public static Tipp<Integer> juhuslikPuu(int h) {
        //[Ülesanne 1]
        if (h < 0)
            return null;
        if (h == 0)
            return new Tipp<Integer>(0, null, null);
        Random r = new Random();
        Integer c = r.nextInt(3);
        switch (c) {
            case 0:
                return new Tipp<Integer>(0, juhuslikPuu(h - 1), juhuslikPuu(h - 1));
            case 1:
                return new Tipp<Integer>(0, juhuslikPuu(h - 2), juhuslikPuu(h - 1));
            default:
                return new Tipp<Integer>(0, juhuslikPuu(h - 1), juhuslikPuu(h - 2));
        }
    }

    public boolean equals(Tipp<T> teine) {
        return onVõrdsed(this, teine);
    }

    private boolean onVõrdsed(Tipp<T> esimene, Tipp<T> teine) {
        if (esimene == null) {
            return teine == null;
        }
        return teine != null &&
                esimene.getInfo().equals(teine.getInfo()) &&
                onVõrdsed(esimene.getVasakAlluv(), teine.getVasakAlluv()) &&
                onVõrdsed(esimene.getParemAlluv(), teine.getParemAlluv());
    }

    public Tipp<T> getVasakAlluv() {
        return vasakAlluv;
    }

    public Tipp<T> getParemAlluv() {
        return paremAlluv;
    }

    public void setInfo(T info) {
        this.info = info;
    }

    public void setVasakAlluv(Tipp<T> vasakAlluv) {
        this.vasakAlluv = vasakAlluv;
    }

    public void setParemAlluv(Tipp<T> paremAlluv) {
        this.paremAlluv = paremAlluv;
    }

    public String toString() {
        return toString(this.kõrgus());
    }

    private String toString(int suurimKõrgus) {
        //[Ülesanne 4]
        String puu = "";
        if (paremAlluv != null)
            puu += paremAlluv.toString(suurimKõrgus);
        puu += toStringRow(suurimKõrgus);
        if (vasakAlluv != null)
            puu += vasakAlluv.toString(suurimKõrgus);

        return puu;
    }

    private String toStringRow(int suurimKõrgus) {
        String rida = "";
        for (int i = 0; i < suurimKõrgus - this.kõrgus(); i++) {
            rida += "-";
        }
        rida += info;
        for (int i = 0; i < this.kõrgus(); i++) {
            rida += "-";
        }
        rida += "\n";
        return rida;
    }
}
