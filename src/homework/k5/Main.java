package homework.k5;

public class Main {

    public static void main(String[] args) {
        Tipp<Integer> puu = haru(7, haru(8, leht(1), leht(1)),haru(2,leht(3),leht(4)));
        System.out.println(puu);
        System.out.println(puu.ul2());
        System.out.println(ul(puu, 0));
        puu.ul3(1);
    }

    //algoritm, mis läbib puu eesjärjestuses ja prindib iga paarisarvulise väärtusega tipu juures kõigi eelnevate paaritu arvuliste väärtustega tippude summa
    private static int ul(Tipp<Integer> t, int s) {
        if (t == null)
            return s;
        if (t.info % 2 == 0)
            System.out.println("summa: " + s);
        else
            s += t.info;
        s = ul(t.vasakAlluv, s);
        s = ul(t.paremAlluv, s);
        return s;

    }

    private static Tipp<Integer> haru(Integer e, Tipp<Integer> v, Tipp<Integer> p) {
        return new Tipp<Integer>(e, v, p);
    }


    private static Tipp<Integer> leht(Integer e) {
        return new Tipp<Integer>(e);
    }

}
