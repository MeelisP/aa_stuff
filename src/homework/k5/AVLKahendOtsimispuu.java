package homework.k5;

public interface AVLKahendOtsimispuu<T extends Comparable>{

    /**
     * Lisab kirje kahendotsimispuusse koos AVL tasakaalustamisega.
     * @param kirje - lisatav kirje
     * @return - viide (alam)puu juurele
     * st viide iseendale või tipule, mis asendab antud tippu puu struktuuris.
     */
    public AVLKahendOtsimispuu<T> lisaKirjeAVL(T kirje);

    /**
     * Eemaldab kirje kahendotsimispuust koos AVL tasakaalustamisega.
     * @param kirje - lisatav kirje
     * @return - viide (alam)puu juurele
     * st viide iseendale või tipule, mis asendab antud tippu puu struktuuris.
     */
    public AVLKahendOtsimispuu<T> eemaldaKirjeAVL(T kirje);

    public String sulgEsitus();
}
