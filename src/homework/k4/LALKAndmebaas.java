package homework.k4;

/** Lahtine adresseerimine lineaarse kompimisega andmebaas */
public class LALKAndmebaas extends Andmebaas {

	private final int kompesamm = 1;
	private Isik[] andmed;

	public LALKAndmebaas(int suurus) {
		super(suurus);
		this.andmed = new Isik[suurus];
	}
	
	@Override
	public void LisaIsik(Isik isik) {
		int koht = hashFN(isik.getId());
		int esimeseKoht = koht;
        while (andmed[koht] != null) {
            koht = liiguKompesammuVorra(koht);
            if (koht == esimeseKoht)
				throw new RuntimeException("LALKAndmebaas sai täis");
		}
		andmed[koht] = isik;
	}

	@Override
	public Isik LeiaIsik(int id) {
		int koht = leiaKoht(id);
		if (koht == -1)
			return null;
		return andmed[koht];
	}

    @Override
	public boolean EemaldaIsik(Isik isik) {
		return EemaldaIsik(isik.getId());
	}

	@Override
	public boolean EemaldaIsik(int id) {
		//kontrollitakse, et kas isik leidub andmebaasis
		int kohtRasis = leiaKoht(id);
		if (kohtRasis == -1)
			return false;

		//Isiku eemaldamine
        andmed[kohtRasis] = null;
        kohtRasis = liiguKompesammuVorra(kohtRasis);

        //elementide asukoha muutmine
        while (andmed[kohtRasis] != null) {
            Isik isik = andmed[kohtRasis];
            andmed[kohtRasis] = null;
            LisaIsik(isik);
            kohtRasis = liiguKompesammuVorra(kohtRasis);
		}
        return true;
	}

    /***
	 *
	 * @param id isiku id
	 * @return tagastab isiku koha arrays. -1 kui ei ole arrays
	 */
	private int leiaKoht(int id) {
		int koht = hashFN(id);
		int esimeseKoht = koht;
		idVordlemisteArv++;
        while (andmed[koht] != null) {

			if (andmed[koht].getId() == id)
				return koht;

			koht = liiguKompesammuVorra(koht);
			idVordlemisteArv++;
			if (koht == esimeseKoht)
			    return -1;
		}
		return -1;
	}

	/***
	 *
	 * @param pos praegune asukoht räsis
	 * @return uue asukoha, mis on saadud 1 korra kompesammu võrra edasi liikudes
	 */
	private int liiguKompesammuVorra(int pos){
		return (pos + kompesamm) % suurus;
	}

    private int hashFN(int k) {
        double T = (Math.sqrt(5) - 1) / 2;
        return (int) Math.floor(suurus * (k * T - Math.floor(k * T)));
    }
}
