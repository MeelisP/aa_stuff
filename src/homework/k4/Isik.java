package homework.k4;

public class Isik {
	
	private int id;
	
	private String nimi;
	
	private int palk;
	
	Isik(int id, String nimi, int palk) {
		this.id = id;
		this.nimi = nimi;
		this.palk = palk;
	}
	
	public int getId() {
		return id;
	}
	
	public String getNimi() {
		return nimi;
	}
	
	public double getPalk() {
		return palk;
	}

	public String toString() {
		return "Isik nr." + id + " " + nimi;
	}
}
