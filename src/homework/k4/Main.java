package homework.k4;

import homework.k4.Isik;

import java.util.*;

public class Main {

    public static void main(String[] args) {
        //yl1();
        //yl2lalk();
        //yl2kimp();
        //yl3kimpSort();
        //ise1otsimiseEfektiivsus();
        ise2sortimisVõrdlus();
    }

    public static void yl1() {

    }

    public static void yl2lalk() {
        Andmebaas lalkAndmebaas = new LALKAndmebaas(10);
        lalkAndmebaas.LisaIsik(new Isik(1, "loom", 1000));
        lalkAndmebaas.LisaIsik(new Isik(13, "loom1", 1000));
        Isik loom = new Isik(111, "loom111", 10000);
        lalkAndmebaas.LisaIsik(loom);
        lalkAndmebaas.LisaIsik(new Isik(12, "loom2", 1000));
        lalkAndmebaas.LisaIsik(new Isik(11, "loom3", 1000));
        System.out.println(lalkAndmebaas.LeiaIsik(111));
        System.out.println(lalkAndmebaas.EemaldaIsik(111));
        System.out.println(lalkAndmebaas.LeiaIsik(111));
        System.out.println(lalkAndmebaas.LeiaIsik(11));

    }

    public static void yl2kimp() {
        Andmebaas kimbuAndmebaas = new KimbuAndmebaas(10);
        kimbuAndmebaas.LisaIsik(new Isik(1, "loom", 1000));
        kimbuAndmebaas.LisaIsik(new Isik(13, "loom1", 1000));
        Isik loom = new Isik(111, "loom111", 10000);
        kimbuAndmebaas.LisaIsik(loom);
        kimbuAndmebaas.LisaIsik(new Isik(12, "loom2", 1000));
        kimbuAndmebaas.LisaIsik(new Isik(11, "loom3", 1000));
        System.out.println(kimbuAndmebaas.LeiaIsik(1));
        System.out.println(kimbuAndmebaas.LeiaIsik(13));
        System.out.println(kimbuAndmebaas.LeiaIsik(111));
        System.out.println(kimbuAndmebaas.LeiaIsik(12));
        System.out.println(kimbuAndmebaas.LeiaIsik(11));
        System.out.println(kimbuAndmebaas.EemaldaIsik(loom));
        System.out.println(kimbuAndmebaas.LeiaIsik(111));
    }

    public static void yl3kimpSort() {
        List<Isik> inimesed = genereeriInimesteJarjend(100, 10);
        List<Isik> inimesed2 = kimpSortIsikud(inimesed, 1);
        System.out.println(inimesed);
        System.out.println(inimesed2);

        List<Integer> arvud = genereeriArvudeJarjend(10000);
        System.out.println(arvud);
        arvud = kimpSort(arvud, 1);
        System.out.println(arvud);

    }

    public static void ise1otsimiseEfektiivsus() {
        int inimesi = 1000;
        List<List<Isik>> isikud = new ArrayList<>();
        List<List<Isik>> vooradIsikud = new ArrayList<>();
        List<LALKAndmebaas> lalkandmebaasid = new ArrayList<>();
        List<KimbuAndmebaas> kimbuandmebaasid = new ArrayList<>();

        //Järjendite täitmine
        for (int i = 0; i < 9; i++)
            isikud.add(genereeriInimesteJarjend((i + 1) * inimesi / 10, 0));
        isikud.add(genereeriInimesteJarjend(inimesi - inimesi / 100, 0));

        for (int i = 0; i < 9; i++)
            vooradIsikud.add(genereeriInimesteJarjend((i + 1) * inimesi / 10, 0));
        vooradIsikud.add(genereeriInimesteJarjend(inimesi - inimesi / 100, 0));

        for (int i = 0; i < isikud.size(); i++) {
            lalkandmebaasid.add(new LALKAndmebaas(inimesi));
            kimbuandmebaasid.add(new KimbuAndmebaas(inimesi));
            for (Isik isik : isikud.get(i)) {
                lalkandmebaasid.get(i).LisaIsik(isik);
                kimbuandmebaasid.get(i).LisaIsik(isik);
            }
        }
        //otsimine
        System.out.println("Edukas otsimine:");
        inimesteOtsimine(isikud, lalkandmebaasid, kimbuandmebaasid);

        System.out.println("Mitteedukas otsimine:");
        inimesteOtsimine(vooradIsikud, lalkandmebaasid, kimbuandmebaasid);
    }

    /***
     * iseseisev ül 2
     * võrdlemiseks kasutan java Collections.sort().
     * selle keerukus "https://stackoverflow.com/questions/2883821/java-collections-sort-performance" põhjal on O(n Log(n))
     */
    public static void ise2sortimisVõrdlus() {
        //aja mõõtmine
        int kordusi = 5;
        for (int i = 1; i < 26; i++) {
            int koguAeg = 0;
            for (int j = 0; j < kordusi; j++) {
                List<Integer> arvud = genereeriArvudeJarjend(i * 10000);
                long t0 = System.nanoTime();
                kimpSort(arvud, 1);
                //Collections.sort(arvud);
                koguAeg += System.nanoTime() - t0;
            }

            System.out.println(koguAeg * 1f / (kordusi*1000000));
            //System.out.println(i + "000 elemendi sorteerimise keskmine aeg on: " +  koguAeg * 1f / kordusi + "ms");
        }
    }

    /**
     * genereerib listi milles on n suvalist isikut
     *
     * @param n       genereeritavate isikute arv
     * @param vahimID väikseim id
     * @return listi n suvalise isikuga
     */
    private static List<Isik> genereeriInimesteJarjend(int n, int vahimID) {
        List<Isik> uuedIsikud = new ArrayList<>();
        for (int i = 0; i < n; i++) {
            int id = (int) (Math.random() * n) + vahimID;
            uuedIsikud.add(new Isik(id, "inimene" + id, id * 2));
        }
        return uuedIsikud;
    }

    /***
     * genereerib listi milles on n suvalist arvu
     * @param n genereeritavate arvude arv
     * @return listi n suvalise arvuga
     */
    private static List<Integer> genereeriArvudeJarjend(int n) {
        List<Integer> arvud = new ArrayList<>();
        for (int i = 0; i < n; i++) {
            arvud.add((int) (Math.random() * 99999));
        }
        return arvud;
    }

    /***
     * tehtud selle baasil https://en.wikipedia.org/wiki/Bucket_sort
     * @param isikud, list, mida tahetakse sorteerida
     * @param c, konstant, millega korrutatakse räsi suurus
     * @return kimbumeetodil sorteeritud list
     */
    public static List<Isik> kimpSortIsikud(List<Isik> isikud, int c) {
        List<List<Isik>> rasi = new ArrayList<>();
        List<Isik> sorteeritud = new ArrayList<>();
        for (int i = 0; i < isikud.size() * c; i++) {
            rasi.add(new LinkedList<>());
        }

        int minID = 1000;
        int maxID = 1000;
        //Leian, vähima ja maksimaalse id, et saaks lineaarse räsi meetodi teha
        for (Isik isik : isikud) {
            int vaadeldavID = isik.getId();
            if (minID > vaadeldavID)
                minID = vaadeldavID;
            else if (maxID < vaadeldavID)
                maxID = vaadeldavID;
        }
        //täidan kimbumeetodil räsi
        for (int i = 0; i < isikud.size() * c; i++)
            rasi.get(hashFN(isikud.get(i).getId(), maxID, minID, isikud.size())).add(isikud.get(i));

        //sorteerin kimbud ja panen isikud sorteeritud listi
        for (List<Isik> rida : rasi) {
            rida.sort(Comparator.comparingInt(Isik::getId));
            sorteeritud.addAll(rida);
        }
        return sorteeritud;
    }

    /***
     * tehtud selle baasil https://en.wikipedia.org/wiki/Bucket_sort
     * @param jarjend, list, mida tahetakse sorteerida
     * @param c, konstant, millega korrutatakse räsi suurus
     * @return kimbumeetodil sorteeritud list
     */
    public static List<Integer> kimpSort(List<Integer> jarjend, double c) {
        List<List<Integer>> rasi = new ArrayList<>();
        List<Integer> sorteeritud = new ArrayList<>();
        for (int i = 0; i < jarjend.size() * c; i++) {
            rasi.add(new ArrayList<>());
        }

        //täidan kimbumeetodil räsi
        for (Integer arv : jarjend) {
                rasi.get(hashFN(arv, rasi.size())).add(arv);
        }

        //sorteerin kimbud ja panen isikud sorteeritud listi
        for (List<Integer> rida : rasi) {
            rida.sort(Comparator.comparingInt(o -> o));
            sorteeritud.addAll(rida);
        }
        return sorteeritud;
    }

    //lineaarne rasi meetod
    private static int hashFN(int id, int maxID, int minID, int inimesteArv) {
        return (int) Math.floor(inimesteArv / (maxID - minID) * (id - minID));
    }

    //lineaarne rasi meetod täisarvude jaoks
    private static int hashFN(int id, int jarjendiSuurus) {
        return (int) Math.floor(jarjendiSuurus / 99999 * (id - 1));
    }

    //abimeetod, mis kuvab inimesteOtsimise informatsiooni
    private static void inimesteOtsimine(List<List<Isik>> isikud, List<LALKAndmebaas> lalkandmebaasid, List<KimbuAndmebaas> kimbuandmebaasid) {
        for (int i = 0; i < isikud.size(); i++) {

            for (Isik isik : isikud.get(i)) {
                lalkandmebaasid.get(i).LeiaIsik(isik.getId());
                kimbuandmebaasid.get(i).LeiaIsik(isik.getId());
            }
            System.out.println("täidetud: " + isikud.get(i).size() / 10 + "%");
            System.out.println("lalk: " + lalkandmebaasid.get(i).getIdVordlemisteArv() * 1f / isikud.get(i).size());
            System.out.println("kimp: " + kimbuandmebaasid.get(i).getIdVordlemisteArv() * 1f / isikud.get(i).size());

            //panen nende andmebaaside id võrdlemiste arvu 0'ks, siis saab neid uuesti kasutada.
            lalkandmebaasid.get(i).setIdVordlemisteArv(0);
            kimbuandmebaasid.get(i).setIdVordlemisteArv(0);
        }
    }
}
