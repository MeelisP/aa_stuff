package homework.k4;

import homework.k4.Isik;

public abstract class Andmebaas {

	protected int idVordlemisteArv;
	protected int suurus;
	
	public Andmebaas(int suurus) {
		this.suurus = suurus;
	}

	/** Lisab isiku andmebaasi isiku id järgi.
	 * @param isik Lisatav töötaja
	 */
	public abstract void LisaIsik(Isik isik);
	
	/** Leiab isiku tema id järgi
	 * @param id Otsitava isiku id
	 * @return Isik, kui isik eksisteerib andmebaasis. Null, kui ei eksisteeri.
	 */
	public abstract Isik LeiaIsik(int id);

	/**
	 * Eemaldab isiku andmebaasist, kui ta andmebaasis eksisteerib.
	 * @param isik Eemaldatav isik
	 * @return True, kui töötaja oli andmebaasis ja ta eemaldati. False kui töötajat ei leitud.
	 */
	public abstract boolean EemaldaIsik(Isik isik);
	
	/**
	 * Eemaldab isiku andmebaasist, kui ta andmebaasis eksisteerib.
	 * @param id Eemaldava isiku Id
	 * @return True, kui töötaja oli andmebaasis ja ta eemaldati. False kui töötajat ei leitud.
	 */
	public abstract boolean EemaldaIsik(int id);

	/**
	 *
	 * @return Tagastab ID võrdlemiste arvu
	 */
	public int getIdVordlemisteArv() {
		return idVordlemisteArv;
	}

	public void setIdVordlemisteArv(int idVordlemisteArv) {
		this.idVordlemisteArv = idVordlemisteArv;
	}
}
