package homework.k4;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Queue;

/** Kimbumeetodiga andmebaas */
public class KimbuAndmebaas extends Andmebaas {

	private List<Queue<Isik>> andmed = new ArrayList<>();

	public KimbuAndmebaas(int suurus) {
		super(suurus);
        for (int i = 0; i < suurus; i++)
            andmed.add(new LinkedList<>());
    }
	
	@Override
	public void LisaIsik(Isik isik) {
		int kohtRasis = hashFN(isik.getId());
		andmed.get(kohtRasis).add(isik);
		
	}

	@Override
	public Isik LeiaIsik(int id) {
        idVordlemisteArv++;
        for (Isik isik : andmed.get(hashFN(id))) {
            if (isik.getId() == id)
                return isik;
            idVordlemisteArv++;
        }
        return null;
	}

    @Override
	public boolean EemaldaIsik(Isik isik) {
		return EemaldaIsik(isik.getId());
	}

	@Override
	public boolean EemaldaIsik(int id) {
        for (Isik isik : andmed.get(hashFN(id))) {
            if (isik.getId() == id) {
                andmed.get(hashFN(id)).remove(isik);
                return true;
            }
        }
        return false;
	}

    private int hashFN(int k) {
        double T = (Math.sqrt(5) - 1) / 2;
        return (int) Math.floor(suurus * (k * T - Math.floor(k * T)));
    }


}
