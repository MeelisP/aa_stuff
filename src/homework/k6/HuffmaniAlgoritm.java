package homework.k6;

import java.io.*;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.util.*;

public class HuffmaniAlgoritm {

    private Tipp koodiPuu;

    /**
     * Map, mis koosneb sümbolitest(võti) ja nende binaarkujust(väärtus), kasutatakse kodeerimisel
     */
    private Map<Character, String> kodeerimisKoodid;

    /**
     * Map, mis koosneb sümboltie binaarkujust(võti) ja nendele vastavatest sümbolitest(väärtus), kasutatakse dekodeerimisel
     */
    private Map<String, Character> dekodeerimisKoodid;

    /**
     * Pakib faili kokku
     *
     * @param fail, mida tahetakse kokku pakkida
     * @throws IOException
     */
    public void pakiFail(String fail) throws IOException {
        List<String> read = Files.readAllLines(new File(fail).toPath());
        String tekst = "";
        for (String rida : read) {
            tekst += rida;
        }
        genereeriKoodid(tekst);
        try (ObjectOutputStream oos = new ObjectOutputStream(new FileOutputStream(new File(fail)))) {
            salvestaKoodipuuFaili(oos);
            salvestaTekstFaili(oos, read);
        }
    }

    /**
     * Pakib faili lahti
     *
     * @param fail, mida tahetakse lahti pakkida
     * @throws IOException,           visatakse kui üritatakse sobimatut faili lahti pakkida
     * @throws ClassNotFoundException
     */
    public void pakiFailLahti(String fail) throws IOException, ClassNotFoundException {

        List<String> read = new ArrayList<>();
        try (ObjectInputStream ois = new ObjectInputStream(new FileInputStream(new File(fail)))) {
            laeKoodipuuFailist(ois);
            while (ois.available() != 0)
                read.add(dekodeeriKood(ois.readUTF()));
        }
        if (read.size() == 0)
            throw new IOException("fail oli tühi või lugemine ebaõnnestus");
        Files.write(new File(fail).toPath(), read, Charset.forName("UTF-8"));

    }

    /**
     * genereerib koodipuu
     *
     * @param tekst, String mille järgi genereeritakse koodipuu
     * @return koodipuu
     */
    public Tipp genereeriKoodipuu(String tekst) {
        Map<Character, Integer> sagedused = sümboliteSagedused(tekst);
        Eelistusjärjekord<Tipp> järjekord = koostaJärjekord(sagedused);
        koodiPuu = koostaKoodipuu(järjekord);
        return koodiPuu;
    }

    /**
     * Genereerib kodeerimis- ja dekodeerimiskoodid
     *
     * @param tekst
     */
    public void genereeriKoodid(String tekst) {
        genereeriKoodipuu(tekst);
        kodeerimisKoodid = new HashMap<>();
        dekodeerimisKoodid = new HashMap<>();
        genereeriKoodid(koodiPuu, "");

    }

    /**
     * Genereerib kodeerimis- ja dekodeerimiskoodid
     *
     * @param koodiPuu, puu mille järgi koodid tehakse
     * @param võti,     anda tühja sõnena
     */
    private void genereeriKoodid(Tipp koodiPuu, String võti) {
        if (koodiPuu.getParemAlluv() == null && koodiPuu.getVasakAlluv() == null) {
            kodeerimisKoodid.put(koodiPuu.getVäärtus(), võti);
            dekodeerimisKoodid.put(võti, koodiPuu.getVäärtus());
        }
        if (koodiPuu.getVasakAlluv() != null)
            genereeriKoodid(koodiPuu.getVasakAlluv(), võti + "1");
        if (koodiPuu.getParemAlluv() != null)
            genereeriKoodid(koodiPuu.getParemAlluv(), võti + "0");
    }

    /**
     * Leiab sümbolite sagedused
     *
     * @param tekst, Sõne, mille järgi leitakse sümbolite sagedused
     * @return sümbolite sagedused
     */
    private Map<Character, Integer> sümboliteSagedused(String tekst) {
        HashMap<Character, Integer> sagedused = new HashMap<>();
        for (char s : tekst.toCharArray()) {
            if (sagedused.containsKey(s))
                sagedused.put(s, sagedused.get(s) + 1);
            else
                sagedused.put(s, 1);
        }
        return sagedused;
    }

    /**
     * Koostab sümbolite sageduste järjekorra (vähem sagedased on ees)
     *
     * @param sagedused, sümbolite sagedused
     * @return Tagastab sümbolite järjekorra
     */
    private Eelistusjärjekord<Tipp> koostaJärjekord(Map<Character, Integer> sagedused) {
        Eelistusjärjekord<Tipp> järjekord = new Kahendkuhi<Tipp>(new ArrayList<>());
        for (Character character : sagedused.keySet()) {
            järjekord.lisaElement(new Tipp(character, sagedused.get(character)));
        }
        return järjekord;
    }

    /**
     * Õpikust lk 85 leitud algoritmi järgi tehtud
     *
     * @param järjekord, sümbolite sageduste järjekord
     * @return koodipuu
     */
    private Tipp koostaKoodipuu(Eelistusjärjekord<Tipp> järjekord) {
        int pikkus = järjekord.getElementideArv();
        for (int i = 0; i < pikkus - 1; i++) {
            Tipp t1 = järjekord.popEsimene();
            Tipp t2 = järjekord.popEsimene();
            Tipp uusTipp = new Tipp('\u0000', t1.getSagedus() + t2.getSagedus(), t1, t2);
            järjekord.lisaElement(uusTipp);
        }
        return järjekord.popEsimene();
    }

    // Salvestab koodipuu faili
    private void salvestaKoodipuuFaili(ObjectOutputStream oos) throws IOException {
        oos.writeObject(koodiPuu);
    }

    // salvestab teksti faili
    private void salvestaTekstFaili(ObjectOutputStream oos, List<String> tekst) throws IOException {
        for (String rida : tekst)
            oos.writeUTF(kodeeriTekst(rida));
    }

    // laeb koodipuu failist
    private void laeKoodipuuFailist(ObjectInputStream ois) throws IOException, ClassNotFoundException {
        koodiPuu = (Tipp) ois.readObject();
        dekodeerimisKoodid = new HashMap<>();
        kodeerimisKoodid = new HashMap<>();
        genereeriKoodid(koodiPuu, "");
    }

    /***
     * Kodeerib teksti kasutades genereeritud või failist laetud koodipuud.
     * @param tekst kodeeritav tekst
     * @return kodeeritud tekst binaaris sõnena (nt "000101111")
     */
    public String kodeeriTekst(String tekst) {
        StringBuilder kodeeritudTekst = new StringBuilder();
        for (char c : tekst.toCharArray()) {
            kodeeritudTekst.append(kodeerimisKoodid.get(c));
        }

        return kodeeritudTekst.toString();
    }

    /***
     * Dekodeerib teksti kasutades genereeritud või failist laetud koodipuud.
     * @param kood dekodeeritav kood (nt "000101111")
     * @return dekodeeritud tekst sõnena
     */
    public String dekodeeriKood(String kood) {
        StringBuilder dekodeeritudTekst = new StringBuilder();
        String k = "";
        for (char c : kood.toCharArray()) {
            k += c;
            if (dekodeerimisKoodid.containsKey(k)) {
                dekodeeritudTekst.append(dekodeerimisKoodid.get(k));
                k = "";
            }
        }
        return dekodeeritudTekst.toString();
    }

}
