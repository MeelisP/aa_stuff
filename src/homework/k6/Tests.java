package homework.k6;

import org.junit.Test;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;


import static org.junit.Assert.assertEquals;


public class Tests {

    @Test
    public void testGenereeriKoodiPuu() {
        HuffmaniAlgoritm h = new HuffmaniAlgoritm();
        Tipp puu = h.genereeriKoodipuu("abbcccdddd");
        assertEquals(10, puu.getSagedus()); //kogupuu sagedus peab olema võrdne tähtede arvuga
        Tipp puu2 = h.genereeriKoodipuu("abcdeabcdabcaba");
        assertEquals(15, puu2.getSagedus());

    }

    @Test
    public void testViiMullinaAlla() {
        List<Integer> l = new ArrayList<>();
        l.add(20);
        l.add(18);
        l.add(21);
        l.add(12);
        l.add(17);
        Kahendkuhi<Integer> kuhi = new Kahendkuhi<>(l);
        kuhi.viiElementMullinaAlla(0);
        List<Integer> eeldusList = new ArrayList<>();
        eeldusList.add(18);
        eeldusList.add(12);
        eeldusList.add(21);
        eeldusList.add(20);
        eeldusList.add(17);
        assertEquals(new Kahendkuhi<Integer>(eeldusList).toString(), kuhi.toString());
    }

    @Test
    public void testPopEsimene() {
        List<Integer> l = new ArrayList<>();
        l.add(11);
        l.add(12);
        l.add(13);
        l.add(14);
        l.add(15);
        Kahendkuhi<Integer> kuhi = new Kahendkuhi<>(l);
        List<Integer> eeldusList = new ArrayList<>();
        eeldusList.add(12);
        eeldusList.add(14);
        eeldusList.add(13);
        eeldusList.add(15);
        Integer arv = kuhi.popEsimene();
        assertEquals(11, (int) arv);
        assertEquals(new Kahendkuhi<Integer>(eeldusList).toString(), kuhi.toString());
    }

    @Test
    public void testKodeerimine() {
        HuffmaniAlgoritm h = new HuffmaniAlgoritm();
        String s = "abbcccdddd";
        h.genereeriKoodid(s);
        String kodeeritud = h.kodeeriTekst(s);
        String dekodeeritud = h.dekodeeriKood(kodeeritud);
        assertEquals(s, dekodeeritud);
        s = "asojkfnaoskdhnaldbhauilsfbhsiodöabnlsdibns";
        h.genereeriKoodid(s);
        kodeeritud = h.kodeeriTekst(s);
        dekodeeritud = h.dekodeeriKood(kodeeritud);
        assertEquals(s, dekodeeritud);
    }

}
