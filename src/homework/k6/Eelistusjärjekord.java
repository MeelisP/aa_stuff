package homework.k6;

import java.util.List;

public interface Eelistusjärjekord<T extends Comparable<T>> {
	
	void lisaElement(T element);

	/**
	 * eemaldab 1. elemendi ja paneb uue selle asemele
	 * @return 1. element
	 */
	T popEsimene();
	
	int getElementideArv();

	void prindi();
}
