package homework.k6;

import java.io.Serializable;

public class Tipp implements Comparable<Tipp>, Serializable {
	private char väärtus;
	//TODO: Implement
	private int sagedus;
	private Tipp vasakAlluv;
	private Tipp paremAlluv;

	@Override
	public int compareTo(Tipp o) {
		return Integer.compare(sagedus, o.sagedus);
	}
	
	public Tipp(char väärtus, int sagedus) {
		this(väärtus, sagedus, null, null);
	}

	public Tipp(char väärtus, int sagedus, Tipp vasakAlluv, Tipp paremAlluv) {
		this.väärtus = väärtus;
		this.sagedus = sagedus;
		this.vasakAlluv = vasakAlluv;
		this.paremAlluv = paremAlluv;
	}

	public char getVäärtus() {
		return väärtus;
	}
	
	public void setVäärtus(char väärtus) {
		this.väärtus = väärtus;
	}
	
	public Tipp getVasakAlluv() {
		return vasakAlluv;
	}

	public void setVasakAlluv(Tipp vasakAlluv) {
		this.vasakAlluv = vasakAlluv;
	}

	public Tipp getParemAlluv() {
		return paremAlluv;
	}

	public void setParemAlluv(Tipp paremAlluv) {
		this.paremAlluv = paremAlluv;
	}
	
	public int getKõrgus() {
		if(vasakAlluv == null && paremAlluv == null) {
			return 0;
		}
		int v = vasakAlluv == null ? 0 : vasakAlluv.getKõrgus();
		int p = paremAlluv == null ? 0 : paremAlluv.getKõrgus();
		return 1 + Math.max(v,  p);
	}

	public int getTippudeArv() {
		int v = vasakAlluv == null ? 0 : vasakAlluv.getTippudeArv();
		int p = paremAlluv == null ? 0 : paremAlluv.getTippudeArv();
		return 1 + v + p;
	}

	public String sulgEsitus() {
		return väärtus + "(" + 
				(vasakAlluv == null ? "_" : vasakAlluv.sulgEsitus()) + "," +
				(paremAlluv == null ? "_" : paremAlluv.sulgEsitus()) + ")";
	}

	public int getSagedus() {
		return sagedus;
	}

	@Override
	public String toString() {
		return "väärtus=" + väärtus +
				", sagedus=" + sagedus + " ";
	}
}
