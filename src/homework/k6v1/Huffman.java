package homework.k6v1;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.HashMap;

public class Huffman {

    HashMap<String, Integer> sagedusteTabel;

    public Huffman(String failiNimi) throws IOException {
        this.sagedusteTabel = koostaSagedusteTabel(new File(failiNimi).toPath());
    }

    private HashMap<String, Integer> koostaSagedusteTabel(Path failiNimi) throws IOException {
        HashMap<String, Integer> tabel = new HashMap<>();
        for (String rida : Files.readAllLines(failiNimi)) {
            for (String sümbol : rida.split("")) {
                if (tabel.containsKey(sümbol))
                    tabel.put(sümbol, tabel.get(sümbol) + 1);
                else
                    tabel.put(sümbol, 1);
            }
        }
        return tabel;
    }


    public HashMap<String, Integer> getSagedusteTabel() {
        return sagedusteTabel;
    }
}
